import Notification from "Components/Notification";
import React, { createContext, Dispatch, useReducer } from "react";

export type NotificationType = {
  id: number;
  type: "info" | "success" | "warning" | "error";
  title?: string;
  message: string;
};

type ActionsMap<Payloads> = {
  [Key in keyof Payloads]: Payloads[Key] extends undefined ? { type: Key } : { type: Key; payload: Payloads[Key] };
};

type NotificationPayloads = {
  ["ADD_NOTI"]: NotificationType;
  ["REMOVE_NOTI"]: { id: number };
};

export type NotificationActions = ActionsMap<NotificationPayloads>[keyof ActionsMap<NotificationPayloads>];

type NotificationContextType = {
  notificationDispatch: Dispatch<NotificationActions>;
};

export const NotificationContext = createContext<NotificationContextType>({} as NotificationContextType);

const NotificationProvider: React.FC = ({ children }) => {
  const [notificationState, notificationDispatch] = useReducer(
    (notificationState: NotificationType[], action: NotificationActions) => {
      switch (action.type) {
        case "ADD_NOTI":
          return [...notificationState, action.payload];
        case "REMOVE_NOTI":
          return notificationState.filter((notification) => notification.id !== action.payload.id);
        default:
          return notificationState;
      }
    },[]
  );

  return (
    <NotificationContext.Provider value={{ notificationDispatch }}>
      <div className="notification-wrapper">
        {notificationState.map((notification) => (
          <Notification dispatch={notificationDispatch} key={notification.id} {...notification} />
        ))}
      </div>
      {children}
    </NotificationContext.Provider>
  );
};

export default NotificationProvider;
