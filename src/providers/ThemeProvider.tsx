import React, { createContext, useEffect, useState } from "react";
import { themeColours } from "Utils/Constants";

type ThemeName = "light" | "dark";
export type ThemeContextType = {
  theme: ThemeName;
  setTheme: (name: ThemeName) => void;
};
export const ThemeContext = createContext<ThemeContextType>({} as ThemeContextType);

type Props = {
  children: React.ReactNode;
};

const ThemeProvider: React.FC<Props> = ({ children }) => {
  const darkOS = window.matchMedia("(prefers-color-scheme: dark)").matches;
  const localTheme = window.localStorage.getItem("theme");

  let currentTheme: "dark" | "light" = darkOS ? "dark" : "light"; // find exactly current theme to avoid redener
  if (localTheme == "dark" || "light") {
    currentTheme = localTheme == "dark" ? "dark" : "light";
  }

  const [themeName, setThemeName] = useState<ThemeName>(currentTheme);

  useEffect(() => {
    if (localTheme) {
      setTheme(localTheme == "dark" ? "dark" : "light");
      window.localStorage.setItem("theme", localTheme == "dark" ? "dark" : "light");
    } else {
      setTheme(darkOS ? "dark" : "light");
      window.localStorage.setItem("theme", darkOS ? "dark" : "light");
    }
  }, []);

  useEffect(() => {
    console.log(`Dark mode ${themeName == "dark" ? "enabled!" : "disabled!"}`);
  }, [themeName]);

  const setTheme = (name: ThemeName) => {
    window.localStorage.setItem("theme", name);
    const colours = themeColours[name];

    for (const key in colours) {
      document.documentElement.style.setProperty(`--${key}`, colours[key as keyof typeof colours]);
    }
    // document.body.style.setProperty("--backgroundColor", themeColours[name].backgroundColor);
    setThemeName(name);
  };

  // const value = React.useMemo(() => ({ theme: themeName, setTheme }), [themeName]); --> not yet

  return <ThemeContext.Provider value={{ theme: themeName, setTheme }}>{children}</ThemeContext.Provider>;
};

export default ThemeProvider;
