import { useNotification } from "Hooks/useNotification";
import React, { createContext, useEffect, useState } from "react";
import { fetchAPI } from "Services/api";

type User = {
  email: string;
  token: string;
  refresh_token: string;
};

export type AuthContextType = {
  user: User | false;
  signin: (email: string, password: string) => Promise<boolean>;
  isAuthenticating: boolean;
  signout: () => void;
  unauthorized: () => void;
};

export const AuthContext = createContext<AuthContextType>({} as AuthContextType);

const AuthProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<User | false>(false);
  const [isAuthenticating, setIsAuthenticating] = useState(true);
  const { makeNoti } = useNotification();

  useEffect(() => {
    const userLocal = localStorage.getItem("user");
    try {
      if (!userLocal) {
        throw new Error();
      }
      const userLocalObj = JSON.parse(userLocal);
      fetch("https://api.jsonapi.co/rest/v1/user/login", {
        // fake --> change to request 1 api need auth, catch: logout, expired token: get new token by refresh token api
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ email: userLocalObj.email, password: "password" }), // fake --> change to refresh token
      })
        .then((res) => {
          return res.json();
        })
        .then((res) => {
          if (res.success) {
            const userInfo: User = {
              email: "fake_email_from_refresh_token@gmail.com",
              token: res.data.token, // new token from api
              refresh_token: "fake_refresh_token",
            };
            localStorage.setItem("user", JSON.stringify(userInfo));
            setUser(userInfo);
          }
        })
        .finally(() => setIsAuthenticating(false));
    } catch (error) {
      setIsAuthenticating(false);
    }
  }, []);

  const signin = async (email: string, password: string) => {
    try {
      const res = await fetchAPI
        .POST("https://api.jsonapi.co/rest/v1/user/login", { email, password })
        .then((res) => res.json());
      if (res.success) {
        const userInfo: User = {
          email,
          token: res.data.token,
          refresh_token: "fake_refresh_token",
        };
        localStorage.setItem("user", JSON.stringify(userInfo));
        setUser(userInfo);
        return true;
      }
    } catch (error) {
      localStorage.removeItem("user");
      setUser(false);
    }
    return false;
  };

  const signout = () => {
    localStorage.removeItem("user");
    setUser(false);
  };

  const unauthorized = () => {
    makeNoti({ type: "error", message: "Unauthorized, please login again!" });
    signout();
  };

  return (
    <AuthContext.Provider value={{ user, signin, isAuthenticating, signout, unauthorized }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
