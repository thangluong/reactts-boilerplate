import PrivateRoute from "Components/PrivateRoute";
import PublicRoute from "Components/PublicRoute";
import AuthProvider from "Providers/AuthProvider";
import NotificationProvider from "Providers/NotificationProvider";
import ThemeProvider from "Providers/ThemeProvider";
import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import "./App.css";
import Home from "./pages/Home";
import Profile from "./pages/Profile";
import SignIn from "./pages/SignIn";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <ThemeProvider>
        <NotificationProvider>
          <AuthProvider>
            <div className="container">
              <Switch>
                <PrivateRoute exact path="/" component={Home} />
                <PrivateRoute exact path="/profile" component={Profile} />
                <PublicRoute exact path="/signin" component={SignIn} />
                <PublicRoute component={SignIn} />
              </Switch>
            </div>
          </AuthProvider>
        </NotificationProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
};

// render(<App />, document.getElementById("root"));
export default App;
