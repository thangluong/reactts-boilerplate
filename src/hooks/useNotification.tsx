import React from "react";
import { NotificationContext } from "../providers/NotificationProvider";

type NewNotiType = {
  type: "success" | "error" | "warning" | "info";
  title?: string;
  message: string;
};

export const useNotification = (): { makeNoti: (props: NewNotiType) => void } => {
  const { notificationDispatch } = React.useContext(NotificationContext);
  const makeNoti = ({ type, title = "", message }: NewNotiType): void => {
    const id = Math.random() * 10000;
    return notificationDispatch({
      type: "ADD_NOTI",
      payload: { id: id, type, title, message },
    });
  };
  return { makeNoti };
};
