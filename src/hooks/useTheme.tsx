import { ThemeContext, ThemeContextType } from "Providers/ThemeProvider";
import React from "react";

export const useTheme = (): ThemeContextType => React.useContext(ThemeContext);
