import { useEffect, useRef, useState } from "react";
import { fetchAPI } from "Services/api";
import { useAuth } from "./useAuth";

type Methods = "GET" | "POST" | "PUT" | "PATCH"; // | "DELETE"

export const useFetch = (
  method: Methods,
  url: string,
  body: Record<string, unknown> | undefined,
  autoLoad = true
): { isFetching: boolean; response: Record<string, unknown> | null; error: string; refetch: () => void } => {
  const [isFetching, setIsFetching] = useState(false);
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [refetchFlag, setRefetchFlag] = useState(false);
  const { unauthorized } = useAuth();

  const autoLoadRef = useRef(autoLoad);
  const refetch = () => setRefetchFlag(!refetchFlag);

  useEffect(() => {
    const abortController = new AbortController();
    const signal = abortController.signal;

    const doFetch = async () => {
      if (!autoLoadRef.current) {
        autoLoadRef.current = true;
        return;
      }
      if (response) setResponse(null);
      setIsFetching(true);
      try {
        const check = (): Promise<Response> => {
          switch (method) {
            case "GET":
              return fetchAPI.GET(url);
            case "POST":
            case "PUT":
            case "PATCH":
              if (body) {
                return fetchAPI.POST(url, body);
              }
              throw "Invalid body!";
            default:
              throw "Invalid method!";
          }
        };
        const res = await check();
        if (res.status === 401) unauthorized();
        const json = await res.json();
        if (!signal.aborted) {
          setResponse(json);
        }
      } catch (error) {
        console.log(error);
        if (!signal.aborted) {
          setError(error);
        }
      } finally {
        if (!signal.aborted) {
          setIsFetching(false);
        }
      }
    };
    doFetch();

    return () => {
      abortController.abort();
    };
  }, [refetchFlag]);

  return { isFetching, response, error, refetch };
};
