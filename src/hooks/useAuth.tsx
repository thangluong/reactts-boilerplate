import { AuthContext, AuthContextType } from "Providers/AuthProvider";
import React from "react";

export const useAuth = (): AuthContextType => React.useContext(AuthContext);
