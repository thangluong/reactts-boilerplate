import Header from "Components/Header";
import { useAlphaFetch } from "Hooks/useAlphaFetch";
import { useAuth } from "Hooks/useAuth";
import { useNotification } from "Hooks/useNotification";
import React, { useState } from "react";
import { Link } from "react-router-dom";

const Home: React.FC = () => {
  type Type = "info" | "success" | "warning" | "error";

  const [notiType, setNotiType] = useState<Type>("info");
  const [notiMessage, setNotiMessage] = useState("");
  const { signout } = useAuth();
  const { makeNoti } = useNotification();

  useAlphaFetch("/1",{})
  return (
    <div>
      <Header hello="User" showSearch />
      <Link to="/profile">Profile</Link>
      <br />
      <select onChange={(e) => setNotiType(e.target.value as Type)}>
        <option value="info">Info</option>
        <option value="success">Success</option>
        <option value="warning">Warning</option>
        <option value="error">Error</option>
      </select>
      <input value={notiMessage} onChange={(e) => setNotiMessage(e.target.value)} placeholder="Noti message..." />
      <button onClick={() => makeNoti({ type: notiType, message: notiMessage })}>Make notification</button>
      <br />
      <button onClick={signout}>Logout</button>
    </div>
  );
};

export default Home;
