import Header from "Components/Header";
import { useAuth } from "Hooks/useAuth";
import { useFetch } from "Hooks/useFetch";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { fetchAPI } from "Services/api";

const Profile: React.FC = () => {
  const { unauthorized } = useAuth();
  const [fullname, setFullname] = useState("");
  const { isFetching, response, refetch } = useFetch(
    "GET",
    "https://cors-anywhere.herokuapp.com/http://dinoipsum.herokuapp.com/api/?format=json&paragraphs=1&words=10",
    undefined,
    false
  );

  const handleGetDetails = () => {
    fetchAPI
      .GET("https://api.jsonapi.co/rest/v1/user/details")
      .then((res) => {
        if (res.status === 401) {
          unauthorized();
        }
        return res.json();
      })
      .then((res) => {
        console.log("details: ", res);
      });
  };

  return (
    <div>
      <Header hello="User" showSearch /> <Link to="/">Home</Link>
      <br />
      <input placeholder="Fullname" value={fullname} onChange={(e) => setFullname(e.target.value)} />
      <input placeholder="Address" />
      <button onClick={handleGetDetails}>Get details</button>
      <button onClick={refetch} disabled={isFetching}>
        Reload
      </button>
      <div>{isFetching ? "true" : "false"}</div>
      {response && <div>{JSON.stringify(response)}</div>}
    </div>
  );
};

export default Profile;
