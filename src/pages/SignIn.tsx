import Header from "Components/Header";
import { useAuth } from "Hooks/useAuth";
import React, { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";

const SignIn: React.FC = () => {
  const [email, setEmail] = useState("abc@gmail.com");
  const [password, setPassword] = useState("password");
  const { state } = useLocation<{ from: { pathname: string } }>();
  const { signin } = useAuth();
  const history = useHistory();

  const handleSignIn = () => {
    const { from } = state || { from: { pathname: "/" } };
    signin(email, password).then((result) => {
      if (result) {
        history.replace(from);
      }
    });
  };

  return (
    <div>
      <Header hello="Guest" />
      <input value={email} placeholder="Your email..." onChange={(e) => setEmail(e.target.value)} />
      <input
        type="password"
        value={password}
        placeholder="Your password..."
        onChange={(e) => setPassword(e.target.value)}
      />
      <button type="submit" onClick={handleSignIn}>
        Sign In
      </button>
    </div>
  );
};

export default SignIn;
