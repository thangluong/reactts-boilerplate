const getHeaders = () => {
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  headers.append("access-control-allow-origin", "*");
  // headers.append("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  // headers.append("Access-Control-Allow-Headers", "*");

  const userLocal = localStorage.getItem("user");
  try {
    if (!userLocal) {
      throw new Error();
    }
    const userLocalObj = JSON.parse(userLocal);
    if (userLocalObj.token) {
      headers.append("token", userLocalObj.token);
    }
  } catch (error) {}

  return headers;
};

const GET = (url: string): Promise<Response> => {
  return fetch(url, { method: "GET", headers: getHeaders() });
};

const POST = (url: string, body: Record<string, unknown>): Promise<Response> => {
  return fetch(url, { method: "POST", headers: getHeaders(), body: JSON.stringify(body) });
};

const PUT = (url: string, body: Record<string, unknown>): Promise<Response> => {
  return fetch(url, { method: "PUT", headers: getHeaders(), body: JSON.stringify(body) });
};

const PATCH = (url: string, body: Record<string, unknown>): Promise<Response> => {
  return fetch(url, { method: "PATCH", headers: getHeaders(), body: JSON.stringify(body) });
};

// do not recommend
// const DELETE = (url: string): Promise<Response> => {
//   return fetch(url, { method: "DELETE", headers: getHeaders() });
// };

export const fetchAPI = { GET, POST, PUT, PATCH /* DELETE */ };
