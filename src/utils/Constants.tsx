export const themeColours = {
  light: {
    color: "black",
    backgroundColor: "white",
    shadowColor: "rgba(0, 0, 0, 0.3)",
  },
  dark: {
    color: "white",
    backgroundColor: "black",
    shadowColor: "rgba(255, 255, 255, 0.3)",
  },
};
