import { useAuth } from "Hooks/useAuth";
import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";

type PublicRouteProps = RouteProps & {
  component: React.FC;
  // layout: React.ReactNode --> not yet
};

const PublicRoute: React.FC<PublicRouteProps> = ({ component: Component, ...rest }) => {
  const userLocal = localStorage.getItem("user");
  const { user, isAuthenticating } = useAuth();
  if (userLocal) {
    return isAuthenticating ? (
      <div>Authenticating...</div>
    ) : user ? (
      <Redirect to={{ pathname: "/" }} />
    ) : (
      <Route {...rest} render={() => <Component />} />
    );
  }
  return <Route {...rest} render={() => <Component />} />;
};

export default PublicRoute;
