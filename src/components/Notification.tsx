import { NotificationActions, NotificationType } from "Providers/NotificationProvider";
import React, { useEffect, useRef, useState } from "react";
import "./Notification.scss";

type Props = NotificationType & {
  dispatch: React.Dispatch<NotificationActions>;
};
const Notification: React.FC<Props> = ({ dispatch, id, type, title, message }) => {
  const [isExit, setIsExit] = useState(false);
  const [cooldownBarHeight, setCooldownBarHeight] = useState(0);
  const intervalID = useRef<NodeJS.Timeout | null>(null);

  const handleStartTimer = () => {
    const id = setInterval(() => {
      setCooldownBarHeight((prevHeight) => {
        if (prevHeight < 100) {
          // < 100%
          return prevHeight + 0.5;
        }

        clearInterval(id);
        return prevHeight;
      });
    }, 20); // 20 * (100 / 0.5) = 4000ms total

    intervalID.current = id;
  };

  const handlePauseTimer = () => {
    if (intervalID.current) {
      clearInterval(intervalID.current);
    }
  };

  const handleResumeTimer = () => {
    if (intervalID.current) {
      handleStartTimer();
    }
  };

  const handleStopTimer = () => {
    handlePauseTimer();
    intervalID.current = null;
  };

  const handleCloseNotification = () => {
    handleStopTimer();
    setIsExit(true);
    setTimeout(() => {
      dispatch({ type: "REMOVE_NOTI", payload: { id } });
    }, 300); // match with Exit keyframes in scss
  };

  useEffect(() => {
    if (cooldownBarHeight >= 100) {
      // >= 100%
      handleCloseNotification();
    }
  }, [cooldownBarHeight]);

  useEffect(() => {
    handleStartTimer();
  }, []);

  return (
    <div
      onMouseEnter={handlePauseTimer}
      onMouseLeave={handleResumeTimer}
      className={`notification-item ${type} ${isExit ? "exit" : ""}`}
    >
      <div className="cooldown-wrapper">
        <div className="notification-icon">ic</div>
        <div className="cooldown-bar" style={{ height: `${100 - cooldownBarHeight}%` }} />
      </div>
      <div className="content-wrapper">
        <div className="notification-close-icon" onClick={handleCloseNotification}>
          ✕
        </div>
        {message}
      </div>
    </div>
  );
};

export default Notification;
