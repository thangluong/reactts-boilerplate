import { useAuth } from "Hooks/useAuth";
import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";

type PrivateRouteProps = RouteProps & {
  component: React.FC;
  // layout: React.ReactNode --> not yet
};

const PrivateRoute: React.FC<PrivateRouteProps> = ({ component: Component, ...rest }) => {
  const { user, isAuthenticating } = useAuth();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticating ? (
          <div>Authenticating...</div>
        ) : user ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: "/signin",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
