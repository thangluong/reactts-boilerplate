import { useTheme } from "Hooks/useTheme";
import React from "react";

interface Props {
  hello: string;
  showSearch?: boolean | undefined;
}

const Header: React.FC<Props> = ({ hello, showSearch }) => {
  const { theme, setTheme } = useTheme();
  console.log("render Header");
  return (
    <div>
      <div>
        Hello {hello}
        {showSearch && (
          <>
            , search: <input />
          </>
        )}
      </div>
      <div>
        <button onClick={() => setTheme(theme == "dark" ? "light" : "dark")}>Change theme!</button>
      </div>
    </div>
  );
};

export default React.memo(Header);
