import { Configuration } from "webpack";
import { join } from "path";
import { rootDir } from "./utils/env";
import TerserJSPlugin from "terser-webpack-plugin";
import * as plugins from "./plugins";
import merge from "webpack-merge";
import base from "./base";

const prod: Configuration = {
  mode: "production",
  target: ["web", "es5"],
  output: {
    path: join(rootDir, "/dist"),
    publicPath: "/",
    filename: "[name].[contenthash:8].js",
  },

  optimization: {
    minimize: true,
    minimizer: [new TerserJSPlugin({})],
  },
  plugins: [plugins.dotenvProdPlugin, plugins.cleanWebpackPlugin, plugins.miniCssExtractPlugin],
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
};

export default merge(prod, base);
