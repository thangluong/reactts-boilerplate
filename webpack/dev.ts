import { Configuration } from "webpack";
import { join } from "path";
import { rootDir } from "./utils/env";
import { devServer, devServerUrl } from "./configs";
import * as plugins from "./plugins";
import merge from "webpack-merge";
import base from "./base";

const dev: Configuration = {
  mode: "development",
  target: "web",
  output: {
    path: join(rootDir, "/dist"),
    publicPath: devServerUrl,
    filename: "[name].[fullhash].js",
  },

  devtool: "cheap-module-source-map",
  devServer: devServer,
  plugins: [plugins.dotenvDevPlugin],
};

export default merge(dev, base);
