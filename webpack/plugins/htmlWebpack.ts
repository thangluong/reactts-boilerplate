import HtmlWebpackPlugin, { Options } from "html-webpack-plugin";
import { join } from "path";
import { rootDir } from "../utils/env";

const config: Options = {
  filename: "index.html",
  inject: true,
  template: join(rootDir, "./public/index.html"),
};

export const htmlWebpackPlugin = new HtmlWebpackPlugin(config);
