import { CleanWebpackPlugin, Options } from "clean-webpack-plugin";

const config: Options = {
  cleanOnceBeforeBuildPatterns: ["**/*", "!profile.json"],
};

export const cleanWebpackPlugin = new CleanWebpackPlugin(config);
