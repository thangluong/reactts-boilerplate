import Dotenv, { Options } from "dotenv-webpack";
import { join } from "path";
import { rootDir } from "../utils/env";

const config: Options = {
  path: join(rootDir, "/.env.prod"),
};

export const dotenvProdPlugin = new Dotenv(config);
