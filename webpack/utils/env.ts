import { join } from "path";

export const rootDir = join(__dirname, "../../");
export const webpackDir = join(__dirname, "../");
