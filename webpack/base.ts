// import path from "path";
import { Configuration } from "webpack";
import * as rules from "./rules";
import * as plugins from "./plugins";
import { aliasItems, entry, optimization } from "./configs";

const base: Configuration = {
  context: __dirname,
  entry,
  module: {
    rules: [
      {
        test: /\.m?js$/,
        resolve: { fullySpecified: false },
      },
      rules.urlLoader,
      rules.babelLoader,
      rules.styleLoader,
      // rules.cssLoader,
    ],
  },
  plugins: [plugins.htmlWebpackPlugin],
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".jsx"],
    alias: aliasItems,
  },
  optimization,
  // externals: externalItems,
};

export default base;
