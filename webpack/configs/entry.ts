import { Entry } from "webpack";
import { join } from "path";
import { rootDir } from "../utils/env";

export const entry: Entry = {
  app: [join(rootDir, "/src/index.tsx")],
};
