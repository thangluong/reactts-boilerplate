import { ResolveOptions } from "webpack";
import { join } from "path";
import { rootDir } from "../utils/env";

export const aliasItems: ResolveOptions["alias"] = {
  // remember to change in tsconfig too
  src: join(rootDir, "/src/"),
  Components: join(rootDir, "/src/components/"),
  Hooks: join(rootDir, "/src/hooks/"),
  Providers: join(rootDir, "/src/providers/"),
  Services: join(rootDir, "/src/services/"),
  Utils: join(rootDir, "/src/utils/"),
};
