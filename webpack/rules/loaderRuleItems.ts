import { RuleSetRule } from "webpack";

export const urlLoader: RuleSetRule = {
  test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
  exclude: /node_modules/,
  use: {
    loader: "url-loader",
    options: {
      limit: 10000,
      name: "static/media/[name].[hash:8].[ext]",
    },
  },
};

export const babelLoader: RuleSetRule = {
  test: /\.(ts|js)x?$/,
  use: {
    loader: "babel-loader",
  },
};

export const styleLoader: RuleSetRule = {
  test: /\.(sa|sc|c)ss$/,
  exclude: /node_modules/,
  use: ["style-loader", "css-loader", "sass-loader"],
};

// not yet supported
// const cssLoader: RuleSetRule = {
//   test: /\.(c|le)ss$/,
//   use: [
//     {
//       loader: "style-loader",
//     },
//     {
//       loader: "css-loader", // translates CSS into CommonJS
//     },
//     {
//       loader: "less-loader", // compiles Less to CSS
//       options: {
//         lessOptions: {
//           // If you are using less-loader@5 please spread the lessOptions to options directly
//           modifyVars: {
//             // example: "text-color": "#b8b8b8",
//           },
//           javascriptEnabled: true,
//         },
//       },
//     },
//   ],
// };
